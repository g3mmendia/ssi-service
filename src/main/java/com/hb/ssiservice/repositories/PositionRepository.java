//--------Marvin Dickson Mendia Calizaya-------
package com.hb.ssiservice.repositories;

import com.hb.ssiservice.model.Position;
import org.springframework.data.repository.CrudRepository;

public interface PositionRepository extends CrudRepository<Position, Long>
{
}
