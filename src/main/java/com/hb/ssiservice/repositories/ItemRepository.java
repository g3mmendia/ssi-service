//--------Marvin Dickson Mendia Calizaya-------
package com.hb.ssiservice.repositories;

import com.hb.ssiservice.model.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long>
{
}
