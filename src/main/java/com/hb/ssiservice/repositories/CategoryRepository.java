//--------Marvin Dickson Mendia Calizaya-------
package com.hb.ssiservice.repositories;

import com.hb.ssiservice.model.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long>
{
}
