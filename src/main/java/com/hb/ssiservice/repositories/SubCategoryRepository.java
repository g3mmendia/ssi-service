//--------Marvin Dickson Mendia Calizaya-------
package com.hb.ssiservice.repositories;

import com.hb.ssiservice.model.SubCategory;
import org.springframework.data.repository.CrudRepository;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long>
{
}
