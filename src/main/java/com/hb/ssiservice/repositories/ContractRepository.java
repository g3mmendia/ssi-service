//--------Marvin Dickson Mendia Calizaya-------
package com.hb.ssiservice.repositories;

import com.hb.ssiservice.model.Contract;
import org.springframework.data.repository.CrudRepository;

public interface ContractRepository extends CrudRepository<Contract, Long>
{
}
