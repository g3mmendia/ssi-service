//--------Marvin Dickson Mendia Calizaya-------
package com.hb.ssiservice;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
public class PersistenceConfig
{
}